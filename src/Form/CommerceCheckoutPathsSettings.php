<?php

namespace Drupal\commerce_checkout_paths\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Settings form for Commerce checkout pages custom paths.
 */
class CommerceCheckoutPathsSettings extends ConfigFormBase {

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames(): array|string {
    return 'commerce_checkout_paths.settings';
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId(): string {
    return 'commerce_checkout_paths_settings';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $form['commerce_checkout_paths_settings'] = [
      '#type'        => 'fieldset',
      '#title'       => t('Commerce checkout paths configuration.'),
      '#description' => t('Set your custom paths for a checkout pages like: shop/step1'),
    ];

    foreach (_commerce_checkout_paths_get_checkout_pages_list() as $name => $data) {
      $title = t($data['title']) . ' <i>(machine_name: ' . t($name) . ')</i>';
      $value = \Drupal::state()->get('commerce_checkout_paths_' . $name, FALSE);
      $form['commerce_checkout_paths_settings']['commerce_checkout_paths_' . $name] = [
        '#type'           => 'textfield',
        '#title'          => $title,
        '#default_value'  => $value,
        '#description'    => $data['description'],
      ];
    }

    $form['commerce_checkout_paths_settings']['token_tree'] = [
      '#type' => 'fieldset',
      '#title' => t('Replacement patterns'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#description' => [
        '#theme' => 'token_tree_link',
        'token_types' => [
          'commerce-order'
        ],
      ],
    ];

    $form['commerce_checkout_paths_settings']['commerce_checkout_paths_auto_redirect'] = [
      '#type'           => 'checkbox',
      '#title'          => t('Apply form redirect alter'),
      '#default_value'  => \Drupal::state()->get('commerce_checkout_paths_auto_redirect', FALSE),
      '#description'    => t('Automatically add a custom form submits with redirect to next/previous step with using custom URLs.'),
    ];

    return $form;
  }

}
